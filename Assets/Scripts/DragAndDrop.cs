using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class DragAndDrop : NetworkBehaviour
{
    private Vector3 _mousePosition;
    private Vector3 _mousePos;
    private Vector3 _lastPos;

    private bool _onSellZone = false;

    private List<Transform> _playerPositions;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("SellZone"))
        {
            _onSellZone = true;
            if (gameObject.GetComponent<CharacterControl>().player.Value == 1)
            {
                if (LevelManager.Instance.Player1)
                {
                    GameManager.Instance.ActivateSellUI(GetComponent<CharacterControl>().Card.price);  
                }
            }else if (gameObject.GetComponent<CharacterControl>().player.Value == 2)
            {
                if (LevelManager.Instance.Player2)
                {
                    GameManager.Instance.ActivateSellUI(GetComponent<CharacterControl>().Card.price);  
                }
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("SellZone"))
        {
            _onSellZone = false;
            if (gameObject.GetComponent<CharacterControl>().player.Value == 1)
            {
                if (LevelManager.Instance.Player1)
                {
                    GameManager.Instance.DisableSellUI(); 
                }
            }else if (gameObject.GetComponent<CharacterControl>().player.Value == 2)
            {
                if (LevelManager.Instance.Player2)
                {
                    GameManager.Instance.DisableSellUI();
                }
            }
            
        }
    }

    private Vector3 GetMousePosition()
    {
        return Camera.main.WorldToScreenPoint(transform.position);
    }

    private void OnMouseDown()
    {
        if (!LevelManager.Instance.inCombat.Value)
        {
            _mousePosition = Input.mousePosition - GetMousePosition();
            _lastPos = new Vector3(transform.position.x, 0, transform.position.z);  
        }
        
    }

    private void OnMouseDrag()
    {
        
        if (!LevelManager.Instance.inCombat.Value)
        {
            _mousePos = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition - _mousePosition).x, 0, Camera.main.ScreenToWorldPoint(Input.mousePosition - _mousePosition).z); 
            MoveCharacterRpc(_mousePos);
        }
        
    }

    private void OnMouseUp()
    {
        if (!LevelManager.Instance.inCombat.Value)
        {
            LevelManager.Instance.RemoveOcupiedPosition(_lastPos);

            Vector3 finalPos = Vector3.zero;

            float minDistance = float.MaxValue;

            if (LevelManager.Instance.Player1)
            {
                _playerPositions = LevelManager.Instance.Player1Positions;
            }
            else if (LevelManager.Instance.Player2)
            {
                _playerPositions = LevelManager.Instance.Player2Positions;
            }

            foreach (Transform pos in _playerPositions)
            {
                NavMeshPath path = new NavMeshPath();
                NavMesh.CalculatePath(_mousePos, pos.position, 1, path);

                var corners = path.corners;
                var fullDistance = 0f;

                for (int i = 1; i < path.corners.Length; i++)
                {
                    fullDistance += Vector3.Distance(corners[i - 1], corners[i]);
                }

                if (minDistance > fullDistance)
                {
                    minDistance = fullDistance;
                    finalPos = new Vector3(pos.position.x, 0.5f, pos.position.z);
                }

            }

            if (!_onSellZone)
            {

                if (!LevelManager.Instance.OcupedPositions.Contains(finalPos))
                {
                    _lastPos = finalPos;
                    LevelManager.Instance.AddToOcupiedPosition(finalPos);
                }
                else
                {
                    transform.position = _lastPos;
                    LevelManager.Instance.AddToOcupiedPosition(_lastPos);
                }

                MoveCharacterRpc(finalPos);
            }
            else
            {
                LevelManager.Instance.AddGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,
                    GetComponent<CharacterControl>().Card.price);
                Debug.Log("Gold to earn " + GetComponent<CharacterControl>().Card.price);
                if (gameObject.GetComponent<CharacterControl>().player.Value == 1)
                {
                    if (LevelManager.Instance.Player1)
                    {
                        GameManager.Instance.AssignGoldUi(LevelManager.Instance.player1Gold.Value);
                        LevelManager.Instance.RemoveFromTotalCharacters(LevelManager.Instance.Player1,
                            LevelManager.Instance.Player2);
                        GameManager.Instance.SetUnitsInField(LevelManager.Instance.player1TotalCharacters.Value,
                            LevelManager.Instance.player1level.Value);
                        GameManager.Instance.DisableSellUI();
                    }
                }
                else if (gameObject.GetComponent<CharacterControl>().player.Value == 2)
                {
                    if (LevelManager.Instance.Player2)
                    {
                        GameManager.Instance.AssignGoldUi(LevelManager.Instance.player2Gold.Value);
                        LevelManager.Instance.RemoveFromTotalCharacters(LevelManager.Instance.Player1,
                            LevelManager.Instance.Player2);
                       
                        GameManager.Instance.SetUnitsInField(LevelManager.Instance.player2TotalCharacters.Value,
                            LevelManager.Instance.player2level.Value);
                        GameManager.Instance.DisableSellUI();
                    }
                }

                DespawnRpc();
            }
        }
    }
    
    [Rpc(SendTo.Server)]
    private void MoveCharacterRpc(Vector3 newPosition)
    {
        transform.position = newPosition;
     
    }
    
    
    [Rpc(SendTo.Server)]
    private void DespawnRpc()
    {
        gameObject.GetComponent<NetworkObject>().Despawn();  
    }
    
    
    
    
    
}