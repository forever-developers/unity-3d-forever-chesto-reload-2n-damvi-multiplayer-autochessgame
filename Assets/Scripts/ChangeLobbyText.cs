using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ChangeLobbyText : MonoBehaviour
{
    [SerializeField]
    private TMP_Text textLobby;
    
    public void ChangeText(String text)
    {
        textLobby.text = text;
    }
    
}
