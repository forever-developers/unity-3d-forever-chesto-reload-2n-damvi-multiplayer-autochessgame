using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadCardData : MonoBehaviour
{
    private CardSO _card;

    [SerializeField] private TMP_Text cardName;
    [SerializeField] private Image cardSprite;
    [SerializeField] private TMP_Text hp;
    [SerializeField] private TMP_Text attack;
    [SerializeField] private TMP_Text defense;
    [SerializeField] private TMP_Text range;
    [SerializeField] private TMP_Text speedAtack;
    [SerializeField] private TMP_Text price;

    public void LoadData(CardSO card)
    {
        cardName.text = card.cardName;
        hp.text = "HP: " + card.hp;
        attack.text = "Attack: " + card.attack;
        defense.text = "Defense: " + card.defense;
        range.text = "Range: " + card.range;
        speedAtack.text = "Speed Attack: " + card.speedAttack;
        price.text = card.price.ToString();
        cardSprite.sprite = card.sprite;
        _card = card;
    }

    public void ClickedCard()
    {
       
        if (LevelManager.Instance.Player1)
        {
      
            if (LevelManager.Instance.player1TotalCharacters.Value <
                LevelManager.Instance.levelScale[LevelManager.Instance.player1level.Value])
            {
                switch (gameObject.tag)
                {
                    case "Card1":
                        if (LevelManager.Instance.player1Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card1ButtonPressed(_card.id); 
                           
                        }
                        else
                        {
                           GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                    case "Card2":
                        if (LevelManager.Instance.player1Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card2ButtonPressed(_card.id); 
                            
                        }
                        else
                        {
                            GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                    case "Card3":
                        if (LevelManager.Instance.player1Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card3ButtonPressed(_card.id); 
                            
                        }
                        else
                        {
                            GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                    case "Card4":
                        if (LevelManager.Instance.player1Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card4ButtonPressed(_card.id); 
                            
                        }
                        else
                        {
                            GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                
                } 
                
            }
            else
            {
                GameManager.Instance.SetInfoText("You don't have space for more characters"); 
            }
             
        }
        else if (LevelManager.Instance.Player2)
        {
            if (LevelManager.Instance.player2TotalCharacters.Value <
                LevelManager.Instance.levelScale[LevelManager.Instance.player2level.Value])
            {
                switch (gameObject.tag)
                {
                    case "Card1":
                        if (LevelManager.Instance.player2Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card1ButtonPressed(_card.id); 
                           
                        }
                        else
                        {
                           GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                    case "Card2":
                        if (LevelManager.Instance.player2Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card2ButtonPressed(_card.id); 
                            
                        }
                        else
                        {
                            GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                    case "Card3":
                        if (LevelManager.Instance.player2Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card3ButtonPressed(_card.id); 
                           
                        }
                        else
                        {
                            GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                    case "Card4":
                        if (LevelManager.Instance.player2Gold.Value>=GameManager.Instance.GetCardData(_card.id).price)
                        {
                            LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1, LevelManager.Instance.Player2,GameManager.Instance.GetCardData(_card.id).price);
                            GameManager.Instance.Card4ButtonPressed(_card.id); 
                            
                        }
                        else
                        {
                            GameManager.Instance.SetInfoText("You don't have enough gold");
                        }
                        break;
                } 
            }
            else
            {
                GameManager.Instance.SetInfoText("You don't have space for more characters");    
            }
        }
    }
        
    
}