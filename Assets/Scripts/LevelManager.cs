using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class LevelManager : NetworkBehaviour
{
    public int Tank { get => _tank; set => _tank = value; }
    public int Ranger { get => _ranger; set => _ranger = value; }
    public int Assasin { get => _assasin; set => _assasin = value; }
    public int Mage { get => _mage; set => _mage = value; }
    

    public static LevelManager Instance = null;

    private bool player1, player2;
    [Header("Camera things")]
    [Space]
    [SerializeField] private GameObject cameraPlayer1;

    private Vector3 _normalCameraPlayer1;
    private Vector3 _normalCameraPlayer2;
    private int _normalCameraProjection;
    public Vector3 combatCameraPlayer1;
    public Vector3 combatCameraPlayer2;
    public int combatCameraProjection;
    [SerializeField] private GameObject cameraPlayer2;
    [Space]
    
    [Header("In game data")]
    [SerializeField] private List<CardSO> cardsList;
    [SerializeField] private List<Transform> player1Positions;
    [SerializeField] private List<Transform> player2Positions;
    [SerializeField] private GameObject characterPrefab;
    public int countdownTime = 20;
    public Dictionary<int, int> levelScale = new Dictionary<int, int>();
    public LayerMask player1Layer;
    public LayerMask player2Layer;
  
    
    
    private NetworkList<Vector3> _ocupedPositions;
    [HideInInspector]
    public NetworkVariable<FixedString64Bytes> player1Name;
    [HideInInspector]
    public NetworkVariable<FixedString64Bytes> player2Name;
    [HideInInspector]
    public NetworkVariable<bool> inCombat;
    [HideInInspector]
    public NetworkVariable<int> player1TotalCharacters;
    [HideInInspector]
    public NetworkVariable<int> player2TotalCharacters;
    [HideInInspector]
    public NetworkVariable<int> player1AliveCharacters;
    [HideInInspector]
    public NetworkVariable<int> player2AliveCharacters;
    [HideInInspector]
    public NetworkVariable<int> player1level;
    [HideInInspector]
    public NetworkVariable<int> player2level;
    [HideInInspector]
    public NetworkVariable<int> player1Gold;
    [HideInInspector]
    public NetworkVariable<int> player2Gold;
    [HideInInspector]
    public NetworkVariable<int> player1Hp;
    [HideInInspector]
    public NetworkVariable<int> player2Hp;

   
    
    private int _tank = 0;
    private int _ranger = 0;
    private int _assasin = 0;
    private int _mage = 0;
    
    
    
    public GameObject CameraPlayer1 { get => cameraPlayer1; set => cameraPlayer1 = value; }
    public GameObject CameraPlayer2 { get => cameraPlayer2; set => cameraPlayer2 = value; }
    public List<Transform> Player1Positions { get => player1Positions; set => player1Positions = value; }
    public List<Transform> Player2Positions { get => player2Positions; set => player2Positions = value; }
    public NetworkList<Vector3> OcupedPositions { get => _ocupedPositions; set => _ocupedPositions = value; }
    public bool Player1 { get => player1; set => player1 = value; }
    public bool Player2 { get => player2; set => player2 = value; }


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        _ocupedPositions = new NetworkList<Vector3>();
        player1TotalCharacters.Value = 0;
        player2TotalCharacters.Value = 0;
        player1AliveCharacters.Value = 0;
        player2AliveCharacters.Value = 0;
        player1level.Value = 1;
        player2level.Value = 1;
        player1Gold.Value = 25;
        player2Gold.Value = 25;
        inCombat.Value = false;
    }

    public override void OnNetworkSpawn()
    {
        SelectShopCards();
        if (IsClient)
        {
            if (player1)
            {
                player2Name.OnValueChanged += OnPlayer2NameChange;
                player1Gold.OnValueChanged += OnPlayer1GoldChanged;
                player1TotalCharacters.OnValueChanged += OnPlayer1UnitsChange;
                player1level.OnValueChanged += OnPlayer1LevelChange;
                player1AliveCharacters.OnValueChanged += OnPlayer1AliveCharacterChange;
                GameManager.Instance.AssignGoldUi(player1Gold.Value);
                SendPlayerNameRpc(GameManager.Instance.PlayerName, true, false);
            }
            else
            {
                player1Name.OnValueChanged += OnPlayer1NameChange;
                player2Gold.OnValueChanged += OnPlayer2GoldChanged;
                player2TotalCharacters.OnValueChanged += OnPlayer2UnitsChange;
                player2level.OnValueChanged += OnPlayer2LevelChange;
                player2AliveCharacters.OnValueChanged += OnPlayer2AliveCharacterChange;
                
                GameManager.Instance.AssignGoldUi(player2Gold.Value); 
                SendPlayerNameRpc(GameManager.Instance.PlayerName, false, true);
            }
        }
        InitializingData();
        inCombat.OnValueChanged += OnInCombatChange;
        player1AliveCharacters.OnValueChanged += OnPlayer1AliveCharacterChange;
        player2AliveCharacters.OnValueChanged += OnPlayer2AliveCharacterChange;
        StartCoroutine(CooldownStartCombat());
    }
    

    private void InitializingData()
    {
        levelScale.Add(1,3);
        levelScale.Add(2,4);
        levelScale.Add(3,6);
        levelScale.Add(4,8);
        levelScale.Add(5,10);
        GameManager.Instance.InitializeSliders();
        _normalCameraPlayer1 = cameraPlayer1.transform.position;
        _normalCameraPlayer2 = cameraPlayer2.transform.position;
        _normalCameraProjection = (int)cameraPlayer1.GetComponent<Camera>().orthographicSize;
        _tank = 0;
        _ranger = 0;
        _assasin = 0;
        _mage = 0;
        GameManager.Instance.ControlUnitsCount(_tank, _ranger, _assasin, _mage);
        
    }
    public void OnPlayer1GoldChanged(int previous, int current)
    {
        GameManager.Instance.AssignGoldUi(player1Gold.Value);
    }
    public void OnPlayer2GoldChanged(int previous, int current)
    {
        GameManager.Instance.AssignGoldUi(current);
    }

    public void OnPlayer1UnitsChange(int previous, int current)
    {
        GameManager.Instance.SetUnitsInField(player1TotalCharacters.Value, player1level.Value);
    }
    public void OnPlayer2UnitsChange(int previous, int current)
    {
        GameManager.Instance.SetUnitsInField(player2TotalCharacters.Value, player2level.Value);
    }

    public void OnPlayer1LevelChange(int prevoius, int current)
    {
        GameManager.Instance.SetLevelText(current);
        GameManager.Instance.SetUnitsInField(player1TotalCharacters.Value, current);
    }
    public void OnPlayer2LevelChange(int prevoius, int current)
    {
        GameManager.Instance.SetLevelText(current);
        GameManager.Instance.SetUnitsInField(player2TotalCharacters.Value, current);
    }

    public void OnPlayer1NameChange(FixedString64Bytes prevoius, FixedString64Bytes current)
    {
        GameManager.Instance.AssignOponentNameOnRightBox(current.ToString());
    }
    public void OnPlayer2NameChange(FixedString64Bytes prevoius, FixedString64Bytes current)
    {
        GameManager.Instance.AssignOponentNameOnRightBox(current.ToString());
    }

    public void OnPlayer1AliveCharacterChange(int previous, int current)
    {
        if (current == 0)
        {
            GameManager.Instance.LoadMenuScene();
            //SceneManager.LoadScene("InitScene");
            NetworkManager.Singleton.SceneManager.LoadScene("InitScene", LoadSceneMode.Single);
            StartCoroutine(OneSecondPause());
            
            
        }
    }

    
    public void OnPlayer2AliveCharacterChange(int previous, int current)
    {
        
        if (current == 0)
        {
            GameManager.Instance.LoadMenuScene();
            //SceneManager.LoadScene("InitScene");
            NetworkManager.Singleton.SceneManager.LoadScene("InitScene", LoadSceneMode.Single);
            StartCoroutine(OneSecondPause());
        } 
    }
   
    IEnumerator OneSecondPause()
    {
        
        if (IsClient)
        {
            Debug.Log("HI");
        }

        
        yield return new WaitForSeconds(2);
        
        
        /*if (IsServer)
        {
            NetworkManager.Singleton.SceneManager.LoadScene("LobbyScene", LoadSceneMode.Single);
        }*/
    }
    public void OnInCombatChange(bool previous, bool current)
    {
        if (current)
        {
            GameManager.Instance.DeactivateBottomBox();
            GameManager.Instance.SetCountdownText("In combat");
            if (player1)
            {
                cameraPlayer1.transform.position = combatCameraPlayer1;
                cameraPlayer1.GetComponent<Camera>().orthographicSize = combatCameraProjection;  
            }else if (player2)
            {
                cameraPlayer2.transform.position = combatCameraPlayer2;
                cameraPlayer2.GetComponent<Camera>().orthographicSize = combatCameraProjection;  
            }
        }
        else
        {
            GameManager.Instance.ActivateBottomBox();
            StartCoroutine(CooldownStartCombat());
            if (player1)
            {
                cameraPlayer1.transform.position = _normalCameraPlayer1;
                cameraPlayer1.GetComponent<Camera>().orthographicSize = _normalCameraProjection;   
            }else if (player2)
            {
                cameraPlayer2.transform.position = _normalCameraPlayer2;
                cameraPlayer2.GetComponent<Camera>().orthographicSize = _normalCameraProjection;  
            }

            
        }
    }

  

    public void SelectShopCards()
    {
        GameManager.Instance.ActivateAllCardButton();
        GameManager.Instance.LoadCardOnBottomBox(cardsList[Random.Range(0, cardsList.Count)],
            cardsList[Random.Range(0, cardsList.Count)],
            cardsList[Random.Range(0, cardsList.Count)],
            cardsList[Random.Range(0, cardsList.Count)]);
    }
    

    public void Spawn(int idCard)
    {
        SpawnRpc(idCard, player1, player2);
    }
    
    [Rpc(SendTo.Server)]
    private void SpawnRpc(int idCard, bool p1, bool p2)
    {
        Vector3 vector = Vector3.zero;
        bool noSpace = false;
        
        if (p1)
        {
            foreach (Transform t in Player1Positions)
            {
                
                Vector3 p = t.position;
                if (!OcupedPositions.Contains(p))
                {
                    vector = p;
                    OcupedPositions.Add(p);
                    player1TotalCharacters.Value += 1;
                    break;
                }
                
            }

        }
        else if (p2)
        {
            foreach (Transform t in Player2Positions)
            {
              
                Vector3 p = t.position;
                if (!OcupedPositions.Contains(p))
                {
                    vector = p;
                    OcupedPositions.Add(p);
                    player2TotalCharacters.Value += 1;
                    break;
                }
            }
        }

        if (!noSpace)
        {
            GameObject character = Instantiate(characterPrefab);
            character.transform.position = vector;
            //Aix� instancia l'objecte per la xarxa, i d'aquesta forma apareixer� tamb� als altres clients connectats.
            //character.GetComponent<CharacterControl>().LoadStats(idCard);
            character.GetComponent<CharacterControl>().SetId(idCard);
            if (p1)
            {
                character.GetComponent<CharacterControl>().player.Value = 1;
              
                character.layer = 6;
             
                character.name = "CharacterP1";
                

            }else if (p2)
            {
                character.GetComponent<CharacterControl>().player.Value = 2;
                
                character.layer = 7;
               
                character.name = "CharacterP2";
               
                
            }
            
            character.GetComponent<NetworkObject>().Spawn();
        }
    }

    public void RemoveAlivePlayer1()
    {
        RemoveAlivePlayer1Rpc();
    }
    
    [Rpc(SendTo.Server)]
    private void RemoveAlivePlayer1Rpc()
    {
        player1AliveCharacters.Value--;
       
    }
    
    public void RemoveAlivePlayer2()
    {
        RemoveAlivePlayer2Rpc();
    }
    
    [Rpc(SendTo.Server)]
    private void RemoveAlivePlayer2Rpc()
    {
        player2AliveCharacters.Value--;
       
    }

    public void AddLevel(bool p1, bool p2)
    {
        AddLevelRpc(p1, p2);
    }
    
    [Rpc(SendTo.Server)]
    private void AddLevelRpc(bool p1, bool p2)
    {
        if (p1)
        {
            player1level.Value++;
        }
        else if (p2)
        {
            player2level.Value++;  
        }
    }


    public void AddToOcupiedPosition(Vector3 position)
    {
        AddToOcupiedPositionRpc(position);
    }
    
    [Rpc(SendTo.Server)]
    private void AddToOcupiedPositionRpc(Vector3 newPosition)
    {
        
        OcupedPositions.Add(newPosition);
    }
    
    [Rpc(SendTo.Server)]
    private void RemoveOcupiedPositionRpc(Vector3 newPosition)
    {
       
      
        OcupedPositions.Remove(newPosition);
        
    }
    public void RemoveOcupiedPosition(Vector3 position)
    {
        RemoveOcupiedPositionRpc(position);
    }
    [Rpc(SendTo.Server)]
    private void SendPlayerNameRpc(FixedString64Bytes name, bool p1, bool p2)
    {
        if (p1)
        {
            player1Name.Value = name;
        }else if (p2)
        {
            player2Name.Value = name;
        }
        
    }

    public void RemoveGold(bool p1, bool p2, int gold)
    {
        
        RemoveGoldRpc(p1, p2, gold);   
    }
    
    [Rpc(SendTo.Server)]
    private void RemoveGoldRpc(bool p1, bool p2, int gold)
    {
       
        if (p1)
        {
            player1Gold.Value -= gold;
        }else if (p2)
        {
            player2Gold.Value -= gold;
        }
        
    }

    public void RemoveFromTotalCharacters(bool p1, bool p2)
    {
        RemoveFromTotalCharactersRpc(p1, p2);
    }

    [Rpc(SendTo.Server)]
    private void RemoveFromTotalCharactersRpc(bool p1, bool p2)
    {
        if (p1)
        {
            player1TotalCharacters.Value--;
        }
        else if(p2)
        {
            player2TotalCharacters.Value--;  
        }
    }
    
    public void AddToTotalCharacters(bool p1, bool p2)
    {
        AddToTotalCharactersRpc(p1, p2);
    }

    [Rpc(SendTo.Server)]
    private void AddToTotalCharactersRpc(bool p1, bool p2)
    {
        if (p1)
        {
            player1TotalCharacters.Value++;
        }
        else if(p2)
        {
            player2TotalCharacters.Value++;  
        }
    }

    public void AddGold(bool p1, bool p2, int gold)
    {
        AddGoldRpc(p1, p2, gold);
    }

    [Rpc(SendTo.Server)]
    private void AddGoldRpc(bool p1, bool p2, int gold)
    {
        if (p1)
        {
            player1Gold.Value += gold;
        }
        else if (p2)
        {
            player2Gold.Value += gold;
        }
        
    }

    IEnumerator CooldownStartCombat()
    {
        for (int i = countdownTime; i>=0; i--)
        {
            yield return new WaitForSeconds(1);
            GameManager.Instance.SetCountdownText(i+"");
        }
        if (IsServer)
        {
            inCombat.Value = true;
            player1AliveCharacters.Value = player1TotalCharacters.Value;
            player2AliveCharacters.Value = player2TotalCharacters.Value;
        }
    }

    public override void OnDestroy()
    {
        if (IsClient)
        {
            if (player1)
            {
                player2Name.OnValueChanged -= OnPlayer2NameChange;
                player1Gold.OnValueChanged -= OnPlayer1GoldChanged;
                player1TotalCharacters.OnValueChanged -= OnPlayer1UnitsChange;
                player1level.OnValueChanged -= OnPlayer1LevelChange;
                player1AliveCharacters.OnValueChanged -= OnPlayer1AliveCharacterChange;
            }
            else
            {
                player1Name.OnValueChanged -= OnPlayer1NameChange;
                player2Gold.OnValueChanged -= OnPlayer2GoldChanged;
                player2TotalCharacters.OnValueChanged -= OnPlayer2UnitsChange;
                player2level.OnValueChanged -= OnPlayer2LevelChange;
                player2AliveCharacters.OnValueChanged -= OnPlayer2AliveCharacterChange;
            }
            inCombat.OnValueChanged -= OnInCombatChange;
        }
        
    }
}