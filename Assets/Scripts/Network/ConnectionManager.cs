using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ConnectionManager : NetworkBehaviour
{
    private NetworkVariable<int> _numberPlayers = new NetworkVariable<int>();
    private int _countDown = 4; 
    
    public override void OnNetworkSpawn()
    {
        Debug.Log("On networkSpawn del lobby");
        SceneManager.sceneLoaded += OnSceneLoaded;
        if (SceneManager.GetActiveScene()==SceneManager.GetSceneByName("LobbyScene"))
        {
        
            base.OnNetworkSpawn();
            if (IsServer)
            {
                _numberPlayers.Value = NetworkManager.Singleton.ConnectedClients.Count;
                //numberOfClients.text += " "+NetworkManager.Singleton.ConnectedClients.Count+"";
                GameManager.Instance.SetLobbyText(NetworkManager.Singleton.ConnectedClients.Count+"");
                if (_numberPlayers.Value == 2)
                {
                    StartCoroutine(LobbyCooldown());
                }
            } 
            if (IsClient)
            {
                if (_numberPlayers.Value == 2)
                {
                
                    GameManager.Instance.SetLobbyText("Game about to start");
                    StartCoroutine(LobbyCooldownClient());
                }
                else if(_numberPlayers.Value!=2)
                {
                    GameManager.Instance.SetLobbyText("Waiting for players "+_numberPlayers.Value);  
                }
            }
        }
    }

    public override void OnNetworkDespawn()
    {
        GameManager.Instance.SetLobbyText("Waiting for players "+_numberPlayers.Value);
    }
    
    IEnumerator LobbyCooldown()
    {
        yield return new WaitForSeconds(_countDown+1);
        NetworkManager.Singleton.SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    IEnumerator LobbyCooldownClient()
    {
        for (int i = _countDown; i >=0 ; i--)
        {
            GameManager.Instance.SetLobbyText("Game about to start in "+i);
            yield return new WaitForSeconds(1);
            
        }
        
    }

    public void OnSceneLoaded(Scene gameScene, LoadSceneMode sceneMode)
    {
     
      
            if (IsOwner)
            {
                GameManager.Instance.LoadGameScene();
                GameManager.Instance.AssignPlayerNameOnRightBox(GameManager.Instance.PlayerName);
                if (OwnerClientId%2==0)
                {
                    LevelManager.Instance.CameraPlayer2.SetActive(!LevelManager.Instance.CameraPlayer2.activeSelf);
                    LevelManager.Instance.Player2 = true;
                    LevelManager.Instance.Player1= false;
                }
                else
                {
                    LevelManager.Instance.CameraPlayer1.SetActive(!LevelManager.Instance.CameraPlayer1.activeSelf);
                    LevelManager.Instance.Player2 = false;
                    LevelManager.Instance.Player1= true;
                }
                GameManager.Instance.ActivateBottomBox();
            }

    }

    public override void OnDestroy()
    {
        base.OnDestroy();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}

