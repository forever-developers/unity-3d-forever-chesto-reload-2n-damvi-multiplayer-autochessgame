using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HandleConnectionUI : MonoBehaviour
{
    [SerializeField] private Button serverButton;
    [SerializeField] private Button clientButton;
    [SerializeField] private TMP_Text nameText;
    [SerializeField] private TMP_Text ipText;
    [SerializeField] private TMP_Text portText;

    void Start()
    {
        if (NetworkManager.Singleton.IsListening)
        {
            NetworkManager.Singleton.Shutdown();
        }
        GameManager.Instance.LoadMenuScene();
        clientButton.onClick.AddListener(() =>
        {
            ModifyValues(GameManager.typesOfConnector.CLIENT);
            GameManager.Instance.LoadLobbyScene();
            
        });

        serverButton.onClick.AddListener(() =>
        {
            ModifyValues(GameManager.typesOfConnector.SERVER);
            GameManager.Instance.LoadLobbyScene();
        });
       
    }

    public void ModifyValues(GameManager.typesOfConnector type)
    {
        if (ipText.text.Length != 1)
            GameManager.Instance.IP = ipText.text;

        if (portText.text.Length != 1)
            GameManager.Instance.Port = ushort.Parse(portText.text.Substring(0, portText.text.Length - 1));

        if (nameText.text.Length != 1)
            GameManager.Instance.PlayerName = nameText.text;

        GameManager.Instance.Type = type;
    }
}