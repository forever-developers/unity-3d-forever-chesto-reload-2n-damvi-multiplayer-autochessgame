using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using Unity.VisualScripting;
using UnityEngine;

public class LobbyConnection : MonoBehaviour
{
    private string _ip;
    private ushort _port;
    private GameManager.typesOfConnector _type;
    
    private void Start()
    {
        
            Debug.Log("Server");
            _ip = GameManager.Instance.IP;
            _port= GameManager.Instance.Port;
            _type = GameManager.Instance.Type;
            GameManager.Instance.SetLobbyNameText(GameManager.Instance.PlayerName);
            if (_type == GameManager.typesOfConnector.CLIENT)
            {
                NetworkManager.Singleton.GetComponent<UnityTransport>().SetConnectionData(
                    _ip,  
                    _port,
                    "0.0.0.0" 
                );  
                NetworkManager.Singleton.StartClient();
            }

            if (_type == GameManager.typesOfConnector.SERVER)
            {
            
                GameManager.Instance.SetLobbyNameText("Server");
                NetworkManager.Singleton.GetComponent<UnityTransport>().SetConnectionData(
                    _ip,  
                    _port,
                    "0.0.0.0" 
                );  
                NetworkManager.Singleton.StartServer();
            } 
        
        
    }

    

    
    
    
}
