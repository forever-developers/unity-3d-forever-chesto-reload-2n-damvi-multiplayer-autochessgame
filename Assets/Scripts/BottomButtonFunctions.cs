using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomButtonFunctions : MonoBehaviour
{
    public void RefreshCards()
    {
        if (LevelManager.Instance.Player1)
        {
            if (LevelManager.Instance.player1Gold.Value>=1)
            {
                LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1,LevelManager.Instance.Player2, 1);
                GameManager.Instance.ActivateAllCardButton();
                LevelManager.Instance.SelectShopCards();
            }else
            {
                GameManager.Instance.SetInfoText("You don't have enough gold");
            }
            
        }else if (LevelManager.Instance.Player2)
        {
            if (LevelManager.Instance.player2Gold.Value>=1)
            {
                LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1,LevelManager.Instance.Player2, 1);
                GameManager.Instance.ActivateAllCardButton();
                LevelManager.Instance.SelectShopCards();
            }else
            {
                GameManager.Instance.SetInfoText("You don't have enough gold");
            }
        } 
        
    }

    public void UpgradeLevel()
    {
        if (LevelManager.Instance.Player1)
        {
            if (LevelManager.Instance.player1level.Value < 5)
            {
                if (LevelManager.Instance.player1Gold.Value>=5)
                {
                    LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1,LevelManager.Instance.Player2, 5);
                    LevelManager.Instance.AddLevel(LevelManager.Instance.Player1,LevelManager.Instance.Player2);
                }else
                {
                    GameManager.Instance.SetInfoText("You don't have enough gold");
                }  
            }
            else
            {
                GameManager.Instance.SetInfoText("You already are at max level"); 
            }
            
        }else if (LevelManager.Instance.Player2)
        {
            if (LevelManager.Instance.player2level.Value < 5)
            {
                if (LevelManager.Instance.player2Gold.Value>=5)
                {
                    LevelManager.Instance.RemoveGold(LevelManager.Instance.Player1,LevelManager.Instance.Player2, 5);
                    LevelManager.Instance.AddLevel(LevelManager.Instance.Player1,LevelManager.Instance.Player2);
                }else
                {
                    GameManager.Instance.SetInfoText("You don't have enough gold");
                }  
            }
            else
            {
                GameManager.Instance.SetInfoText("You already are at max level"); 
            }
            
        }
    }
}
