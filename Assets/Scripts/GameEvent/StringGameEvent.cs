using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/StringGameEvent")]
public class StringGameEvent : GameEvent<String>
{
   
}
