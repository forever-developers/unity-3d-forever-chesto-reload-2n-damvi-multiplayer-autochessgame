using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class CharacterControl : NetworkBehaviour
{
    private CardSO _card;
    private NetworkVariable<int> _idCard = new NetworkVariable<int>();
    public NetworkVariable<int> player = new NetworkVariable<int>();
    private int _hp;
    private int _attack;
    private int _defense;
    private float _range;
    private float _speedAtack;
    private int _moveSpeed;
    private int _sellPrice;
    private NavMeshAgent agent;
    private bool end = false;
    private bool canAttack = true;
    
    public CardSO Card { get => _card; set => _card = value; }
    public NetworkVariable<int> cardId = new NetworkVariable<int>();
    

    public override void OnNetworkSpawn()
    {
        
        _card = GameManager.Instance.GetCardData(_idCard.Value);
        LoadStats();
        if (player.Value == 1)
        {
            if (LevelManager.Instance.Player1)
            {
                switch (_card.TypeOfCard)
                {
                    case TypeOfCard.MAGE:
                        LevelManager.Instance.Mage++;
                        break;
                    case TypeOfCard.ASSASIN:
                        LevelManager.Instance.Assasin++;
                        break;
                    case TypeOfCard.TANK:
                        LevelManager.Instance.Tank++;
                        break;
                    case TypeOfCard.RANGER:
                        LevelManager.Instance.Ranger++;
                        break;
                }  
            }
            
        }else if (player.Value == 2)
        {
            if (LevelManager.Instance.Player2)
            {
                switch (_card.TypeOfCard)
                {
                    case TypeOfCard.MAGE:
                        LevelManager.Instance.Mage++;
                        break;
                    case TypeOfCard.ASSASIN:
                        LevelManager.Instance.Assasin++;
                        break;
                    case TypeOfCard.TANK:
                        LevelManager.Instance.Tank++;
                        break;
                    case TypeOfCard.RANGER:
                        LevelManager.Instance.Ranger++;
                        break;
                }  
            }
        }
        
        GameManager.Instance.ControlUnitsCount(LevelManager.Instance.Tank, LevelManager.Instance.Ranger, LevelManager.Instance.Assasin, LevelManager.Instance.Mage);
    }

    private void Update()
    {
        if (IsServer)
        {
            if (LevelManager.Instance.inCombat.Value)
            {
                Collider closestEnemy = null;
                float closestDistance = Mathf.Infinity;
                
                LayerMask enemyLayerMask=0;
                if (player.Value == 1)
                {
                    enemyLayerMask = LevelManager.Instance.player2Layer;
                }else if (player.Value == 2)
                {
                    enemyLayerMask = LevelManager.Instance.player1Layer;  
                }

                Collider[] hitColliders = Physics.OverlapSphere(transform.position, Mathf.Infinity, enemyLayerMask);
                /*foreach (var VARIABLE in hitColliders)
                {
                    Debug.Log("Collidersssssss --> "+VARIABLE.gameObject.name);
                    Debug.Log("LayerCollidersssssss --> "+VARIABLE.gameObject.layer);
                }*/
                // Encontrar el enemigo más cercano dentro del rango
               
                foreach (var collider in hitColliders)
                {
                    float distanceToEnemy = Vector3.Distance(transform.position, collider.transform.position);
                    if (distanceToEnemy < closestDistance)
                    {
                        closestDistance = distanceToEnemy;
                        closestEnemy = collider;
                    }
                }
                
                if (closestEnemy != null)
                {
                    
                    agent.SetDestination(closestEnemy.transform.position);
                    agent.speed = _moveSpeed;
                    if (agent.remainingDistance <= _range)
                    {
                        agent.speed=0;
                       
                        if (canAttack&&closestEnemy is not null)
                        {
                            closestEnemy.GetComponent<CharacterControl>().LooseHp(_attack);
                            StartCoroutine(AttackCooldown());
                        }
                        
                    }
                }
                else
                {
                    EndClientRpc();
                    DespawnThisObjectRpc();
                    
                    
                }
                
            }  
        }
        else
        {
            if (end)
            {
                Debug.Log("alsdkhaksjhdaklñsgbdsd");
                GameManager.Instance.LoadMenuScene();
                SceneManager.LoadScene("InitScene");
            }
        }
        
    }
    
    [ClientRpc]
    public void EndClientRpc()
    {
        end = true;
    }
    IEnumerator AttackCooldown()
    {
            canAttack = false;
            yield return new WaitForSeconds(_speedAtack);
            canAttack = true;
    }

    public void LooseHp(int dmg)
    {
        _hp -= dmg;
        if (_hp <= 0)
        {
            if (gameObject != null && gameObject.TryGetComponent(out NetworkObject networkObject))
            {
                networkObject.Despawn();
            }
        }
    }
    
    public void SetId(int id)
    {
        _idCard.Value = id;
    }

    public void LoadStats()
    {
        this._hp = _card.hp;
        this._attack = _card.attack;
        this._defense = _card.defense;
        this._range = _card.range;
        this._speedAtack = _card.speedAttack;
        this._sellPrice = _card.price;
        this._moveSpeed = _card.moveSpeed;
        this.GetComponent<MeshRenderer>().material = _card.material;
        agent = GetComponent<NavMeshAgent>();

    }
    [Rpc(SendTo.Server)]
    private void DespawnThisObjectRpc()
    {
        
       GetComponent<NetworkObject>().Despawn();
       player.Value = 3;
    }
    public override void OnNetworkDespawn()
    {
        
        if (!LevelManager.Instance.inCombat.Value)
        {
            if (player.Value == 1)
            {
                
                if (LevelManager.Instance.Player1)
                {
                    switch (_card.TypeOfCard)
                    {
                        case TypeOfCard.MAGE:
                            LevelManager.Instance.Mage--;
                            break;
                        case TypeOfCard.ASSASIN:
                            LevelManager.Instance.Assasin--;
                            break;
                        case TypeOfCard.TANK:
                            LevelManager.Instance.Tank--;
                            break;
                        case TypeOfCard.RANGER:
                            LevelManager.Instance.Ranger--;
                            break;
                    }  
                }
            
            }else if (player.Value == 2)
            {
                if (LevelManager.Instance.Player2)
                {
                    switch (_card.TypeOfCard)
                    {
                        case TypeOfCard.MAGE:
                            LevelManager.Instance.Mage--;
                            break;
                        case TypeOfCard.ASSASIN:
                            LevelManager.Instance.Assasin--;
                            break;
                        case TypeOfCard.TANK:
                            LevelManager.Instance.Tank--;
                            break;
                        case TypeOfCard.RANGER:
                            LevelManager.Instance.Ranger--;
                            break;
                    }  
                }
            }
            GameManager.Instance.ControlUnitsCount(LevelManager.Instance.Tank, LevelManager.Instance.Ranger, LevelManager.Instance.Assasin, LevelManager.Instance.Mage);
        }
        else
        {
            if (IsServer)
            {
                
                if (player.Value == 1)
                {
                    LevelManager.Instance.RemoveAlivePlayer1();
                }else if (player.Value == 2)
                {
                    LevelManager.Instance.RemoveAlivePlayer2();
                }
            }
               
        }
        
    }
}

