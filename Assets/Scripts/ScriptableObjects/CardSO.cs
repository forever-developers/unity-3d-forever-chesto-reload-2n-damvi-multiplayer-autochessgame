using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "Unit_", menuName = "CharacterCard")]
public class CardSO : ScriptableObject
{
    
    [Header("ID, Name, Image and Level")]
    [Space] 
    public int id;
    [Space] 
    public Sprite sprite;
    [Space]
    public string cardName;
    [Space]
    public int level;
    [Space]
    public TypeOfCard TypeOfCard;
    
    [Header("Unit Stats")]
    [Min(1)]
    [Space]
    public int hp = 1;
    [Space]
    public int attack;
    [Space]
    public int defense;
    [Range(1, 10)]
    [Space]
    public float range;
    [Space]
    public float speedAttack;
    [Space]
    public int moveSpeed;
    [Space]
    public int price;
    [Space] 
    [SerializeField] public Material material;

}
public enum TypeOfCard
{
    TANK, MAGE, ASSASIN, RANGER
}

