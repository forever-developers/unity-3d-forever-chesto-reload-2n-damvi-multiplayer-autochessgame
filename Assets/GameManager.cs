using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    
#region LobbyCanvas

    [Header("Lobby Canvas")] [Space] [SerializeField]
    private GameObject lobbyCanvas;

    [Header("Lobby Info")] [Space] [SerializeField]
    private TMP_Text textName;

    [SerializeField] private TMP_Text lobbyText;

    public void SetLobbyNameText(String name)
    {
        textName.text = name;
    }

    public void SetLobbyText(String name)
    {
        lobbyText.text = name;
    }

    public bool firstTimeLobby;

#endregion

#region Menu Canvas

    [Header("Menu Canvas")] [Space] [SerializeField]
    private GameObject menuCanvas;

#endregion

#region GameCanvas

    [Header("Game Canvas")] [Space] 
    [SerializeField] private GameObject gameCanvas;

    [Header("Global info")] [Space] 
    [SerializeField] private TMP_Text countDown;
    [SerializeField] private TMP_Text infoText;

    [Header("Left box")] [Space] 
    [SerializeField] private TMP_Text tankTextField;
    [SerializeField] private TMP_Text rangerTextField;
    [SerializeField] private TMP_Text assassinTextField;
    [SerializeField] private TMP_Text mageTextField;
    [SerializeField] private GameObject tankIcon;
    [SerializeField] private GameObject rangerIcon;
    [SerializeField] private GameObject assassinIcon;
    [SerializeField] private GameObject mageIcon;

    [Header("Right box")] [Space] 
    [SerializeField] private TMP_Text namePlayer;
    [SerializeField] private TMP_Text nameOponent;
    [SerializeField] private Slider hpSliderPlayer;
    [SerializeField] private Slider hpSliderOponent;

    [Header("Bottom box")] [Space] 
    [SerializeField] private GameObject sellPriceBox;
    [SerializeField] private GameObject listOfCard;
    [SerializeField] private TMP_Text textSellPrice;
    [SerializeField] private GameObject buttonCard1;
    [SerializeField] private GameObject buttonCard2;
    [SerializeField] private GameObject buttonCard3;
    [SerializeField] private GameObject buttonCard4;
    [SerializeField] private LoadCardData firstCardLoad;
    [SerializeField] private LoadCardData secondCardLoad;
    [SerializeField] private LoadCardData thirdCardLoad;
    [SerializeField] private LoadCardData fourthCardLoad;
    [SerializeField] private TMP_Text goldText;
    [SerializeField] private TMP_Text levelText;
    [SerializeField] private TMP_Text unitsInField;
    [SerializeField] private GameObject bottomButtons;
    [SerializeField] private GameObject bottomBox;
    

    public void AssignPlayerNameOnRightBox(String name)
    {
        namePlayer.text = name;
    }
    public void AssignOponentNameOnRightBox(String name)
    {
        nameOponent.text = name;
    }

    public void LoadCardOnBottomBox(CardSO card1, CardSO card2, CardSO card3, CardSO card4)
    {
        firstCardLoad.LoadData(card1);
        secondCardLoad.LoadData(card2);
        thirdCardLoad.LoadData(card3);
        fourthCardLoad.LoadData(card4);
    }

    public void AssignGoldUi(int gold)
    {
        
        goldText.text = gold+"";
    }

    public void ControlUnitsCount(int tank, int rangers, int assasins, int mage)
    {
        if (tank > 0)
        {
            tankIcon.SetActive(true);
            tankTextField.text = tank + "";
        }
        else
        {
            tankIcon.SetActive(false);
            tankTextField.text = 0+"";
        }
        
        
        if (rangers > 0)
        {
            rangerIcon.SetActive(true);
            rangerTextField.text = rangers + "";
        }
        else
        {
            rangerIcon.SetActive(false);
            rangerTextField.text = 0+"";
        }
        
        if (assasins > 0)
        {
            assassinIcon.SetActive(true);
            assassinTextField.text = assasins + "";
        }
        else
        {
            assassinIcon.SetActive(false);
            assassinTextField.text = 0+"";
        }
        
        if (mage > 0)
        {
            mageIcon.SetActive(true);
            mageTextField.text = mage + "";
        }
        else
        {
            mageIcon.SetActive(false);
            mageTextField.text = 0+"";
        }
        
    }

    public void ActivateSellUI(int sellPrice)
    {
        listOfCard.SetActive(false);
        sellPriceBox.SetActive(true);
        textSellPrice.text = "Sells for " + sellPrice;
    }

    public void DisableSellUI()
    {
        listOfCard.SetActive(true);
        sellPriceBox.SetActive(false);
        
    }

    public void SetInfoText(string infoText)
    {
        this.infoText.text = infoText;
        StartCoroutine(InfoTextCooldown());
    }

    IEnumerator InfoTextCooldown()
    {
        yield return new WaitForSeconds(2);
        this.infoText.text = "";
    }

    public void SetCountdownText(string text)
    {
        countDown.text = text;
    }

    public void SetUnitsInField(int numberUnits, int level)
    {
        switch (level)
        {
            case 1:
                
                unitsInField.text = numberUnits + "/3";
                break;
            case 2:
                unitsInField.text = numberUnits + "/4";
                break;
            case 3:
                unitsInField.text = numberUnits + "/6";
                break;
            case 4:
                unitsInField.text = numberUnits + "/8";
                break;
            case 5:
                unitsInField.text = numberUnits + "10";
                break;
        }
    }

    public void SetLevelText(int level)
    {
        levelText.text = "Lv." + level;
    }

    public void InitializeSliders()
    {
        hpSliderPlayer.value = 5;
        hpSliderOponent.value = 5;
    }

    public void RemoveHpPlayer()
    {
        hpSliderPlayer.value--;
    }
    public void RemoveHpOponent()
    {
        hpSliderOponent.value--;
    }

    public void DeactivateBottomBox()
    {
        bottomButtons.SetActive(false);
        bottomBox.SetActive(false);
    }

    public void ActivateBottomBox()
    {
        bottomButtons.SetActive(true);
        bottomBox.SetActive(true);
    }
#endregion

#region NetConfig

    //Config de red
    private string _ip = "127.0.0.1";
    private ushort _port = 7777;
    private string _playerName = "Player";

    public enum typesOfConnector
    {
        CLIENT,
        HOST,
        SERVER
    }

    private typesOfConnector _type;

    public typesOfConnector Type { get => _type; set => _type = value; }

    public string IP { get => _ip; set => _ip = value; }

    public ushort Port { get => _port; set => _port = value; }

    public string PlayerName { get => _playerName; set => _playerName = value; }

#endregion

#region Awake

    public static GameManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }

#endregion

#region SceneManager

    private string _menu = "InitScene";
    private string _lobby = "LobbyScene";
    private string _gameScene = "GameScene";

    public void LoadLobbyScene()
    {
        SceneManager.LoadScene(_lobby);
        IntoLobbyScene();
    }

    public void LoadMenuScene()
    {
        // SceneManager.LoadScene(_menu);
        IntoMenuScene();
    }

    public void LoadGameScene()
    {
        // SceneManager.LoadScene(_gameScene);
        IntoGameScene();
    }
    
    public void IntoLobbyScene()
    {
        
        menuCanvas.SetActive(false);
        gameCanvas.SetActive(false);
        lobbyCanvas.SetActive(true);
    }

    public void IntoMenuScene()
    {
        menuCanvas.SetActive(true);
        lobbyCanvas.SetActive(false);
        gameCanvas.SetActive(false);
    }

    public void IntoGameScene()
    {
        lobbyCanvas.SetActive(false);
        gameCanvas.SetActive(true);
    }

#endregion

#region Buttons

    public void Card1ButtonPressed(int idCard)
    {
        // cosas del network
        LevelManager.Instance.Spawn(idCard);
        
        buttonCard1.SetActive(false);
    }
    
    public void Card2ButtonPressed(int idCard)
    {
        LevelManager.Instance.Spawn(idCard);
        // cosas del network
        buttonCard2.SetActive(false);
    }
    
    public void Card3ButtonPressed(int idCard)
    {
        LevelManager.Instance.Spawn(idCard);
        // cosas del network
        buttonCard3.SetActive(false);
    }
    
    public void Card4ButtonPressed(int idCard)
    {
        LevelManager.Instance.Spawn(idCard);
        // cosas del network
        buttonCard4.SetActive(false);
    }

    public void ActivateAllCardButton()
    {
        buttonCard1.SetActive(true);
        buttonCard2.SetActive(true);
        buttonCard3.SetActive(true);
        buttonCard4.SetActive(true);
    }

#endregion

#region Card "PokeDex"

    [SerializeField] private List<CardSO> cardDex;

    public CardSO GetCardData(int id)
    {
        foreach (var card in cardDex)
        {
            if (card.id == id)
            {
                return card;
            }
        }
        return null;
    }
    
#endregion

}